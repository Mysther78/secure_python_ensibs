#coding:utf-8
import sys
import os.path
import re
import math


def main(line):
	"""
	"""
	try:
		myfloat = parse(line)
		mysqrt = calcul_racine(myfloat)
		print(myfloat, "-->", mysqrt)
	except Exception as e:
		print("Erreur :", e)



def parse(line):
	"""
	"""
	line = line.replace("\"","").replace(",",".").strip()
	
	if not re.match("^\s*-?\s*\d+(\.\d+)?\s*$", line):
		raise Exception("BAD INPUT ("+line+")")

	return float(line)


def calcul_racine(myfloat):
	"""
	"""
	if myfloat < 0:
		raise ValueError("Il n'est pas possible de faire une racine d'un nombre negatif")
	return math.sqrt(myfloat)




if __name__ == "__main__":
	if len(sys.argv) == 2 and os.path.isfile(sys.argv[1]):
		f = open(sys.argv[1])
		lines = f.readlines()
		f.close()
		for line in lines:
			main(line)
	else:
		raise Exception("Fichier d'entrée invalide.")

