import main	
import unittest

class TestExo01(unittest.TestCase):
	
	def test_parse_int(self):
		self.assertEqual(main.parse("1"), 1)
	
	def test_parse_float(self):
		self.assertEqual(main.parse("1.1"), 1.1)
		
	def test_parse_coma(self):
		self.assertEqual(main.parse("1,1"), 1.1)
		
	def test_parse_string(self):
		with self.assertRaises(Exception):
			main.parse("a1")
		with self.assertRaises(Exception):
			main.parse("1a")
		with self.assertRaises(Exception):
			main.parse("1.a")
		with self.assertRaises(Exception):
			main.parse("a.1")
						
	def test_parse_no_val(self):
		with self.assertRaises(Exception):
			main.parse("")
	
	
	def test_calcul_racine(self):
		self.assertEqual(main.calcul_racine(4),2)	
		self.assertEqual(main.calcul_racine(0),0)	
		
	def test_calcul_vitesse_temps_error(self):
		with self.assertRaises(ValueError):
			main.calcul_racine(-4,0)

	
if __name__ == '__main__':
    unittest.main()
