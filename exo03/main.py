#coding:utf-8
import sys
import os.path
import re
import math


def main(line):
	"""
	"""
	try:
		word1, word2 = parse(line)
		s_word = smallest_word(word1, word2)
		print(word1+";"+word2, "-->", s_word)
	except Exception as e:
		print("Erreur :", e)



def parse(line):
	"""
	"""
	line = line.replace("\"","").replace(",",".").strip()
	
	if not re.match("^\s*[^;\s\W]+\s*;\s*[^;\s\W]+\s*$", line):
		raise Exception("BAD INPUT ("+line+")")

	array_val = line.split(";")
	word1 = array_val[0].strip()
	word2 = array_val[1].strip()
	
	return word1, word2


def smallest_word(word1, word2):
	"""
	"""
	if word1 == word2:
		raise Exception("Words are similars")
	return word1 if word1<word2 else word2




if __name__ == "__main__":
	if len(sys.argv) == 2 and os.path.isfile(sys.argv[1]):
		f = open(sys.argv[1])
		lines = f.readlines()
		f.close()
		for line in lines:
			main(line)
	else:
		raise Exception("Fichier d'entrée invalide.")

