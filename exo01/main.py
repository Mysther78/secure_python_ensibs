#coding:utf-8
import sys
import os.path
import re


def main(line):
	"""
		Argument(s):
		- String
		Output(s):
		- None
		Exception(s): 
		- None
	"""
	try:
		distance, temps = parse(line)
		vitesse = calcul_vitesse(distance, temps)
		print(str(distance)+"/"+str(temps), "-->", vitesse)
	except Exception as e:
		print("Erreur :", e)



def parse(line):
	"""
		Argument(s):
		- String 
		Output(s):
		- Array[int, int]
		Exception(s): 
		- "BAD INPUT" : Your string in input have to be in this format '{float};{float}'
	"""
	line = line.replace("\"","").replace(",",".").strip()
	
	if not re.match("^\s*-?\s*\d+(\.\d+)?\s*;\s*\d+(\.\d+)?\s*$", line):
		raise Exception("BAD INPUT ("+line+")")

	array_val = line.split(";")
	distance = float(array_val[0])
	temps = float(array_val[1])
	return distance, temps


def calcul_vitesse(distance, temps): 
	"""
		Argument(s):
		- String 
		Output(s):
		- Array[int, int]
		Exception(s): 
		- "BAD INPUT" : Your string in input have to be in this format '{int};{int}'
	"""
	if temps <= 0:
		raise Exception("Le temps ne peut être divisée par zéro ou moins.")
	if distance < 0:
		raise Exception("La distance ne peut être negative.")
	return round(distance/temps,1)




if __name__ == "__main__":
	if len(sys.argv) == 2 and os.path.isfile(sys.argv[1]):
		f = open(sys.argv[1])
		lines = f.readlines()
		f.close()
		for line in lines:
			main(line)
	else:
		raise Exception("Fichier d'entrée invalide.")

