import main	
import unittest

class TestExo01(unittest.TestCase):
	
	def test_parse_int(self):
		self.assertEqual(main.parse("1;1"), (1,1))
	
	def test_parse_float(self):
		self.assertEqual(main.parse("1.1;1.1"), (1.1,1.1))
		
	def test_parse_coma(self):
		self.assertEqual(main.parse("1,1;1,1"), (1.1,1.1))
		
	def test_parse_string(self):
		with self.assertRaises(Exception):
			main.parse("a.1;1.1")
		with self.assertRaises(Exception):
			main.parse("1.a;1.1")
		with self.assertRaises(Exception):
			main.parse("1.1;a.1")
		with self.assertRaises(Exception):
			main.parse("1.1;1.a")
		with self.assertRaises(Exception):
			main.parse("8e9;9")
			
	def test_parse_3vals(self):
		with self.assertRaises(Exception):
			main.parse("1;2;3")
			
	def test_parse_1val(self):
		with self.assertRaises(Exception):
			main.parse("1;")
		with self.assertRaises(Exception):
			main.parse("1")
		with self.assertRaises(Exception):
			main.parse("")
	
	
	def test_calcul_vitesse(self):
		self.assertEqual(main.calcul_vitesse(10,2),5)	
		self.assertEqual(main.calcul_vitesse(0,2),0)	
		
	def test_calcul_vitesse_temps_error(self):
		with self.assertRaises(Exception):
			main.calcul_vitesse(10,0)
		with self.assertRaises(Exception):
			main.calcul_vitesse(10,-1)
					
	def test_calcul_vitesse_distance_error(self):
		with self.assertRaises(Exception):
			main.calcul_vitesse(-1,2)

	
if __name__ == '__main__':
    unittest.main()
